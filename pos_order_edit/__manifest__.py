# -*- coding: utf-8 -*-
# Copyright 2021 Giniatullin Almas <https://it-projects.info/team/almas50>
# License MIT (https://opensource.org/licenses/MIT).
{
    "name": """POS Order Edit""",
    "summary": """Test""",
    "category": "Point of Sale",
    "images": [],
    "version": "12.0.1.0.2",
    "application": False,
    "author": "IT-Projects LLC, Giniatullin Almas",
    "support": "apps@itpp.dev",
    "website": "https://apps.odoo.com/apps/modules/12.0/pos_order_edit/",
    "license": "Other OSI approved licence",  # MIT
    "depends": ["point_of_sale"],
    "external_dependencies": {"python": [], "bin": []},
    "data": ['views/views.xml'],
    "qweb": [],
    "post_load": None,
    "pre_init_hook": None,
    "post_init_hook": None,
    "auto_install": False,
    "installable": True,
}
