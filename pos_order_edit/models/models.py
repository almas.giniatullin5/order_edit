# -*- coding: utf-8 -*-
from odoo import api, fields, models, tools, _
from odoo.tools import float_is_zero
from odoo.exceptions import UserError
import json

class PodOrderEdit(models.Model):
    _inherit = "pos.order"
    date_order = fields.Datetime(string='Order Date', readonly=True, index=True, default=fields.Datetime.now,
                                 states={'draft': [('readonly', False)]})
    force = fields.Boolean('Is Force Edit', default=False)
    is_invoiced = fields.Boolean("Was invoice", default=False)

    def force_edit(self):
        if self.state == 'invoiced':
            return self.write({'state': 'draft', 'force': True, 'is_invoiced': True})
        return self.write({'state': 'draft', 'force': True})

    @api.multi
    def action_pos_order_paid(self):
        if not self.test_paid():
            raise UserError(_("Order is not paid."))
        if self.is_invoiced and self.force:
            self.create_picking()
            self.update_invoice_and_move()
            self.write({'state': 'invoiced', 'is_invoiced': False})
            return False
        self.write({'state': 'paid'})
        return self.create_picking()

    def update_invoice_and_move(self):
        state = self.invoice_id.state
        self.invoice_id.write({'state': 'draft'})
        for lines in self.invoice_id.invoice_line_ids:
            lines.sudo().unlink()
        for line in self.lines:
            self._action_create_invoice_line(line, self.invoice_id)

        self.invoice_id.sudo().compute_taxes()

        self.account_move.journal_id.update_posted = True

        for lines in self.account_move.line_ids:
            lines.with_context(check_move_validity=False).write({'force': True})

        self.account_move.sudo().button_cancel()


        for lines in self.account_move.line_ids:
            if lines.product_id or lines.tax_line_id:
                lines.with_context(check_move_validity=False).unlink()
        inv = self.invoice_id
        company_currency = inv.company_id.currency_id

        # create move lines (one per invoice line + eventual taxes and analytic lines)


        iml = inv.invoice_line_move_line_get()
        iml += inv.tax_line_move_line_get()

        diff_currency = inv.currency_id != company_currency
        # create one move line for the total and possibly adjust the other lines amount

        total, total_currency, iml = inv.compute_invoice_totals(company_currency, iml)
        name = inv.name or ''
        part = self.env['res.partner']._find_accounting_partner(inv.partner_id)
        line = [(0, 0, self.invoice_id.line_get_convert(l, part.id)) for l in iml]
        line = inv.finalize_invoice_move_lines(line)
        sum_c = 0
        max_d = 0
        for lines in self.account_move.line_ids:
            if lines.debit > max_d:
                max_d = lines.debit
        for lines in self.invoice_id.invoice_line_ids:
            sum_c += lines.price_total

        for lines in self.account_move.line_ids:
            if lines.debit == max_d:
                lines.with_context(check_move_validity=False).write({'debit': sum_c})
        self.account_move.with_context(check_move_validity=False).write({'line_ids': line})
        self.account_move.action_post()
        self.account_move.journal_id.update_posted = False
        self.invoice_id.write({'state': state})
        if state == 'paid' and self.invoice_id.has_outstanding:
            outstanding_payments = json.loads(self.invoice_id.outstanding_credits_debits_widget)
            for line_id in outstanding_payments["content"]:
                self.invoice_id.assign_outstanding_credit(line_id["id"])
            self.invoice_id.pay_and_reconcile(self.env['account.journal'].search([('type', '=', 'cash')], limit=1), self.invoice_id.residual, self.invoice_id.date)
            self.invoice_id.write({'state': 'paid'})
        if state == 'paid' and self.invoice_id.residual!=0:
            self.invoice_id.pay_and_reconcile(self.env['account.journal'].search([('type', '=', 'cash')], limit=1), self.invoice_id.residual, self.invoice_id.date)
            self.invoice_id.write({'state': 'paid'})
            if self.invoice_id.state != 'paid':
                raise UserError(_('something wrong.'))
        return True

    def create_picking(self):
        """Create a picking for each order and validate it."""
        Picking = self.env['stock.picking']
        # If no email is set on the user, the picking creation and validation will fail be cause of
        # the 'Unable to log message, please configure the sender's email address.' error.
        # We disable the tracking in this case.
        if not self.env.user.partner_id.email:
            Picking = Picking.with_context(tracking_disable=True)
        Move = self.env['stock.move']
        StockWarehouse = self.env['stock.warehouse']
        for order in self:
            if order.force:
                order.write({'force': False})
                Pick = order.picking_id
                if not order.lines.filtered(lambda l: l.product_id.type in ['product', 'consu']):
                    continue
                address = order.partner_id.address_get(['delivery']) or {}
                picking_type = order.picking_type_id
                return_pick_type = order.picking_type_id.return_picking_type_id or order.picking_type_id
                moves = Move
                location_id = order.location_id.id
                if order.partner_id:
                    destination_id = order.partner_id.property_stock_customer.id
                else:
                    if (not picking_type) or (not picking_type.default_location_dest_id):
                        customerloc, supplierloc = StockWarehouse._get_partner_locations()
                        destination_id = customerloc.id
                    else:
                        destination_id = picking_type.default_location_dest_id.id

                if picking_type:
                    message = _(
                        "This transfer has been created from the point of sale session: <a href=# data-oe-model=pos.order data-oe-id=%d>%s</a>") % (
                                  order.id, order.name)
                    picking_vals = {
                        'origin': order.name,
                        'partner_id': address.get('delivery', False),
                        'date_done': order.date_order,
                        'picking_type_id': picking_type.id,
                        'company_id': order.company_id.id,
                        'move_type': 'direct',
                        'note': order.note or "",
                        'location_id': location_id,
                        'location_dest_id': destination_id,
                    }
                    pos_qty = any([x.qty > 0 for x in order.lines if x.product_id.type in ['product', 'consu']])
                    if pos_qty:
                        order_picking = Pick.write(picking_vals.copy())
                    neg_qty = any([x.qty < 0 for x in order.lines if x.product_id.type in ['product', 'consu']])
                    if neg_qty:
                        return_vals = picking_vals.copy()
                        return_vals.update({
                            'location_id': destination_id,
                            'location_dest_id': return_pick_type != picking_type and return_pick_type.default_location_dest_id.id or location_id,
                            'picking_type_id': return_pick_type.id
                        })
                        return_picking = Pick.write(return_vals)

                for pick_move in Pick.move_lines:
                    pick_move.write({'state': 'draft'})
                    pick_move.sudo().unlink()
                for line in order.lines.filtered(
                        lambda l: l.product_id.type in ['product', 'consu'] and not float_is_zero(l.qty,
                                                                                                  precision_rounding=l.product_id.uom_id.rounding)):
                    moves |= Move.create({
                        'name': line.name,
                        'product_uom': line.product_id.uom_id.id,
                        'picking_id': Pick.id,
                        'picking_type_id': picking_type.id if line.qty >= 0 else return_pick_type.id,
                        'product_id': line.product_id.id,
                        'product_uom_qty': abs(line.qty),
                        'state': 'draft',
                        'location_id': location_id if line.qty >= 0 else destination_id,
                        'location_dest_id': destination_id if line.qty >= 0 else return_pick_type != picking_type and return_pick_type.default_location_dest_id.id or location_id,
                    })
                    moves._action_done()

                order._force_picking_done(Pick)
                continue
            if not order.lines.filtered(lambda l: l.product_id.type in ['product', 'consu']):
                continue
            address = order.partner_id.address_get(['delivery']) or {}
            picking_type = order.picking_type_id
            return_pick_type = order.picking_type_id.return_picking_type_id or order.picking_type_id
            order_picking = Picking
            return_picking = Picking
            moves = Move
            location_id = order.location_id.id
            if order.partner_id:
                destination_id = order.partner_id.property_stock_customer.id
            else:
                if (not picking_type) or (not picking_type.default_location_dest_id):
                    customerloc, supplierloc = StockWarehouse._get_partner_locations()
                    destination_id = customerloc.id
                else:
                    destination_id = picking_type.default_location_dest_id.id

            if picking_type:
                message = _(
                    "This transfer has been created from the point of sale session: <a href=# data-oe-model=pos.order data-oe-id=%d>%s</a>") % (
                              order.id, order.name)
                picking_vals = {
                    'origin': order.name,
                    'partner_id': address.get('delivery', False),
                    'date_done': order.date_order,
                    'picking_type_id': picking_type.id,
                    'company_id': order.company_id.id,
                    'move_type': 'direct',
                    'note': order.note or "",
                    'location_id': location_id,
                    'location_dest_id': destination_id,
                }
                pos_qty = any([x.qty > 0 for x in order.lines if x.product_id.type in ['product', 'consu']])
                if pos_qty:
                    order_picking = Picking.create(picking_vals.copy())
                    if self.env.user.partner_id.email:
                        order_picking.message_post(body=message)
                    else:
                        order_picking.sudo().message_post(body=message)
                neg_qty = any([x.qty < 0 for x in order.lines if x.product_id.type in ['product', 'consu']])
                if neg_qty:
                    return_vals = picking_vals.copy()
                    return_vals.update({
                        'location_id': destination_id,
                        'location_dest_id': return_pick_type != picking_type and return_pick_type.default_location_dest_id.id or location_id,
                        'picking_type_id': return_pick_type.id
                    })
                    return_picking = Picking.create(return_vals)
                    if self.env.user.partner_id.email:
                        return_picking.message_post(body=message)
                    else:
                        return_picking.sudo().message_post(body=message)

            for line in order.lines.filtered(
                    lambda l: l.product_id.type in ['product', 'consu'] and not float_is_zero(l.qty,
                                                                                              precision_rounding=l.product_id.uom_id.rounding)):
                moves |= Move.create({
                    'name': line.name,
                    'product_uom': line.product_id.uom_id.id,
                    'picking_id': order_picking.id if line.qty >= 0 else return_picking.id,
                    'picking_type_id': picking_type.id if line.qty >= 0 else return_pick_type.id,
                    'product_id': line.product_id.id,
                    'product_uom_qty': abs(line.qty),
                    'state': 'draft',
                    'location_id': location_id if line.qty >= 0 else destination_id,
                    'location_dest_id': destination_id if line.qty >= 0 else return_pick_type != picking_type and return_pick_type.default_location_dest_id.id or location_id,
                })

            # prefer associating the regular order picking, not the return
            order.write({'picking_id': order_picking.id or return_picking.id})

            if return_picking:
                order._force_picking_done(return_picking)
            if order_picking:
                order._force_picking_done(order_picking)

            # when the pos.config has no picking_type_id set only the moves will be created
            if moves and not return_picking and not order_picking:
                moves._action_assign()
                moves.filtered(lambda m: m.product_id.tracking == 'none')._action_done()

        return True


class AccountMoveLineEdit(models.Model):
    _inherit = "account.move.line"
    force = fields.Boolean('Is Force Edit', default=False)

    @api.multi
    def _update_check(self):
        """ Raise Warning to cause rollback if the move is posted, some entries are reconciled or the move is older than the lock date"""
        move_ids = set()
        for line in self:
            err_msg = _('Move name (id): %s (%s) (%s)') % (line.move_id.name, str(line.move_id.id), str(line.force))
            if line.move_id.state != 'draft':
                raise UserError(_('You cannot do this modification on a posted journal entry, you can just change some non legal fields. You must revert the journal entry to cancel it.\n%s.') % err_msg)
            if line.reconciled and not (line.debit == 0 and line.credit == 0) and line.force==False:
                raise UserError(_('You cannot do this modification on a reconciled entry. You can just change some non legal fields or you must unreconcile first.\n%s.') % err_msg)
            if line.move_id.id not in move_ids:
                move_ids.add(line.move_id.id)
        self.env['account.move'].browse(list(move_ids))._check_lock_date()
        return True
